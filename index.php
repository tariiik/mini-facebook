<?php
include('functions/connect.php');
include('functions/ibi.func.php');
$page=htmlentities($_GET['page']);
include("functions/".$page.'.func.php');
$pages=scandir("pages");


if(!empty($page)  && in_array($_GET['page'].".php",$pages))
{
$content='pages/'.$_GET['page'].".php";
}

else
{
header("Location:index.php?page=login");

}
if(isset($_SESSION['pseudo']) && $page!='membre' && $page!='update' &&$page!='update_photo'
&& $page!='liste_membre' && $page!='profile' && $page!='envoi' && $page!='annuler'
&& $page!='invitations' && $page!='accepter' && $page!='refuser' && $page!='amis'
&& $page!='supprimer_amis' && $page!='new_message' && $page!='conversations'
&& $page!='message')
{
header('Location:index.php?page=membre');
}
?>
<html>
<head>
<link  rel="stylesheet" href="css/style.css">
<link  rel="stylesheet" href="css/index.css">
</head>
<body>
<div id='content'>

<?php include($content); ?>
</div>
</body>
</html>
